const express = require("express");
const cors = require('cors');
const path = require("path");
const mongoose = require('mongoose');

// Khởi tạo Express App
const app = express();

const port = 8000;

const {
    creatingOrder,
    gettingDrinks
} = require("./app/controllers/app.controller");

const drinkRouter = require("./app/routes/drinkRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const userRouter = require("./app/routes/userRouter");
const orderRouter = require("./app/routes/orderRouter");

//Cấu hình để sử dụng json
app.use(express.json());

app.use(express.static(__dirname + "/app/views/index"))

// Khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
    .then(() => {
        console.log("Connect mongoDB Successfully");
    })
    .catch((err) => {
        console.log(err);
    });

// Khai báo APi dạng Get "/" sẽ chạy vào đây
// app.get("/", (request, response) => {
//     console.log(__dirname);

//     response.sendFile(path.join(__dirname + "/app/views/index/pizza365index.html"))
// })

app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/app/views/sample.06restAPI.order.pizza365.v2022Nov.html"))
});

app.get("/devcamp-pizza365/drinks", gettingDrinks);
app.post("/devcamp-pizza365/orders", creatingOrder);

// Sử dụng router 
app.use("/api/v1/drinks", drinkRouter);
app.use("/api/v1/vouchers", voucherRouter);
app.use("/api/v1/users", userRouter);
app.use("/api/v1/orders", orderRouter);

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})