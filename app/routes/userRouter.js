const express = require("express");

const router = express.Router();

const {
    getAllUsersMiddleware,
    createUserMiddleware,
    getUserByIDMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
} = require("../middlewares/userMiddleware");

const {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById,
    getLimitUser,
    getSkipUser,
    getSortUser,
    getSkipLimitUser,
    getSortSkipLimitUser
} = require("../controllers/user.controller");

router.get("/", getAllUsersMiddleware, getAllUsers);

router.post("/", createUserMiddleware, createUser);

router.get("/limit-users", getLimitUser);

router.get("/skip-users", getSkipUser);

router.get("/sort-users", getSortUser);

router.get("/skip-limit-users", getSkipLimitUser);

router.get("/sort-skip-limit-users", getSortSkipLimitUser);

router.get("/:userId", getUserByIDMiddleware, getUserById);

router.put("/:userId", updateUserMiddleware, updateUserById);

router.delete("/:userId", deleteUserMiddleware, deleteUserById);

module.exports = router;

