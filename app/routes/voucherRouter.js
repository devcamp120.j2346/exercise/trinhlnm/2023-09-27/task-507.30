const express = require("express");

const router = express.Router();

const {
    getAllVouchersMiddleware,
    createVoucherMiddleware,
    getVoucherByIDMiddleware,
    updateVoucherMiddleware,
    deleteVoucherMiddleware
} = require("../middlewares/voucherMiddleware");

const {
    createVoucher,
    getAllVouchers,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
} = require("../controllers/voucher.controller");

router.get("/", getAllVouchersMiddleware, getAllVouchers);

router.post("/", createVoucherMiddleware, createVoucher);

router.get("/:voucherId", getVoucherByIDMiddleware, getVoucherById);

router.put("/:voucherId", updateVoucherMiddleware, updateVoucherById);

router.delete("/:voucherId", deleteVoucherMiddleware, deleteVoucherById);

module.exports = router;

