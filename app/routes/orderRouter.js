const express = require("express");

const router = express.Router();

const {
    getAllOrdersMiddleware,
    createOrderMiddleware,
    getOrderByIDMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
} = require("../middlewares/orderMiddleware");

const {
    createOrder,
    getAllOrders,
    getOrderById,
    updateOrder,
    deleteOrder
} = require("../controllers/order.controller");

router.get("/", getAllOrdersMiddleware, getAllOrders);

router.post("/", createOrderMiddleware, createOrder);

router.get("/:orderId", getOrderByIDMiddleware, getOrderById);

router.put("/:orderId", updateOrderMiddleware, updateOrder);

router.delete("/:orderId", deleteOrderMiddleware, deleteOrder);

module.exports = router;

