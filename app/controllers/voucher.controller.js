const voucherModel = require("../models/voucher.model");
const mongoose = require("mongoose");

const createVoucher = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        maVoucher,
        phanTramGiamGia,
        ghiChu
    } = req.body;

    // B2: Validate du lieu
    if (!maVoucher) {
        return res.status(400).json({
            message: "Ma voucher khong hop le"
        })
    }

    if (phanTramGiamGia <= 0) {
        return res.status(400).json({
            message: "Phan tram giam gia khong hop le"
        })
    }

    try {
        // B3: Xu ly du lieu

        var newVoucher = {
            maVoucher: maVoucher,
            phanTramGiamGia: phanTramGiamGia,
            ghiChu: ghiChu
        }

        const result = await voucherModel.create(newVoucher);

        return res.status(201).json({
            message: "Tao voucher thanh cong",
            data: result
        })
    } catch (error) {
        // Dung cac he thong thu thap loi de thu thap error

        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllVouchers = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await voucherModel.find();

        return res.status(200).json({
            message: "Lay danh sach voucher thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getVoucherById = async (req, res) => {
    // B1: Thu thap du lieu
    const voucherId = req.params.voucherId;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "Voucher ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await voucherModel.findById(voucherId);

        if(result) {
            return res.status(200).json({
                message: "Lay thong tin voucher thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin voucher"
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateVoucherById = async (req, res) => {
    // B1: Thu thap du lieu
    const voucherId = req.params.voucherId;
    const {
        maVoucher,
        phanTramGiamGia,
        ghiChu
    } = req.body;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "Voucher ID khong hop le"
        })
    }

    if (phanTramGiamGia <= 0) {
        return res.status(400).json({
            message: "Phan tram giam gia khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateVoucher = {};
        if(maVoucher) {
            newUpdateVoucher.maVoucher = maVoucher;
        }
        if(phanTramGiamGia) {
            newUpdateVoucher.phanTramGiamGia = phanTramGiamGia;
        }
        if(ghiChu) {
            newUpdateVoucher.ghiChu = ghiChu;
        }

        const result = await voucherModel.findByIdAndUpdate(voucherId, newUpdateVoucher);

        if(result) {
            return res.status(200).json({
                message: "Update thong tin voucher thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin voucher"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}   

const deleteVoucherById = async (req, res) => {
    // B1: Thu thap du lieu
    const voucherId = req.params.voucherId;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: "Voucher ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await voucherModel.findByIdAndRemove(voucherId);

        if(result) {
            return res.status(200).json({
                message: "Xoa thong tin voucher thanh cong"
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin voucher"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

module.exports = {
    createVoucher,
    getAllVouchers,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}
