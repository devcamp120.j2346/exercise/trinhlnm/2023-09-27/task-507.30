const mongoose = require('mongoose');
const orderModel = require('../models/order.model');
const userModel = require('../models/user.model');
const drinkModel = require('../models/drink.model');

const creatingOrder = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        fullName,
        email,
        address,
        phone,

        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status
    } = req.body;

    // B2: Validate dữ liệu
    if (!fullName) {
        return res.status(400).json({
            message: "Yêu cầu full name"
        })
    }

    if (!email) {
        return res.status(400).json({
            message: "Yêu cầu email"
        })
    }
    if (validateEmail(email) == false) {
        return res.status(400).json({
            message: "Email không đúng định dạng!"
        })
    }

    if (!address) {
        return res.status(400).json({
            message: "Yêu cầu address"
        })
    }

    if (!phone) {
        return res.status(400).json({
            message: "Yêu cầu phone"
        })
    }
    if (validatePhone(phone) == false) {
        return res.status(400).json({
            message: "Phone không đúng định dạng!"
        })
    }

    if (voucher && !mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "voucher ID is not valid"
        })
    }

    if (drink && !mongoose.Types.ObjectId.isValid(drink)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "drink ID is not valid"
        })
    }

    if (!pizzaSize) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizza size is not valid"
        })
    }

    if (!pizzaType) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "pizza type is not valid"
        })
    }

    if (!status) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "status is not valid"
        })
    }
    // B3: Thao tác với CSDL
    try {
        var newOrder = {
            orderCode: Math.random().toString(16).slice(2),
            pizzaSize: pizzaSize,
            pizzaType: pizzaType,
            voucher: voucher,
            drink: drink,
            status: status,
        }

        const createdOrder = await orderModel.create(newOrder);

        var updatedUser;

        const findUserResult = await userModel.findOne({ email: email });

        if (findUserResult) {

            updatedUser = await userModel.findByIdAndUpdate(findUserResult._id, {
                $push: { orders: createdOrder._id }
            })

        } else {

            var newUser = {
                fullName: fullName,
                email: email,
                address: address,
                phone: phone
            }
    
            const resultCreateUser = await userModel.create(newUser);

            updatedUser = await userModel.findByIdAndUpdate(findUserResult._id, {
                $push: { orders: createdOrder._id }
            })
        }

        return res.status(201).json({
            status: "Create order successfully",
            user: updatedUser,
            order: createdOrder
        })

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            status: "Internal server error"
        })
    }
}

//hàm kiểm tra số điện thoại
function validatePhone(paramPhone) {
    // Số điện thoại phải nhập là số, có dấu + hoặc số 0 ở đầu; độ dài từ 10 đến 12 ký tự

    if (isNaN(paramPhone)) {
        return false;
    }

    if (paramPhone.charAt(0) != "0" && paramPhone.charAt(0) != "+") {
        return false;
    }

    if (paramPhone.length < 10 || paramPhone.length > 12) {
        return false;
    }

    return true;
}

// hàm kiểm tra email đúng định dạng ko?
function validateEmail(paramEmail) {
    var vValidRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail.match(vValidRegex)) {
        return true;
    } else {
        return false;
    }
}

const gettingDrinks = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await drinkModel.find();

        return res.status(200).json({
            status: "Get drinks successfully",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error"
        })
    }
}

module.exports = {
    creatingOrder,
    gettingDrinks
}