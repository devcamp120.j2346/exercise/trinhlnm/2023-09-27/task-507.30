const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const orderSchema = new Schema({
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: { //S,M,L
        type: String,
        required: true
    },
    pizzaType: { //Seafood, Hawai, Bacon
        type: String,
        required: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "Drink"
    },
    status: { //confirmed, cancel, open
        type: String,
        required: true
    },
}, {
    timestamps: true
});

module.exports = mongoose.model("Order", orderSchema);
